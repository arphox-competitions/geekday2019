1) LOTS OF breakfast stuff is left. Feel free to grab as many as you like :)

2) HELP NEEDED: A colleague of ours has to empty out his room. He has LOTS of stuff that has to be thrown out into the garbage. There are ~10 cardboard boxes and ~6 big grey plastic bags of random things on FLOOR 3, IN FRONT OF THE ELEVATOR. If you feel the power (and you want to help), please go up to floor 3, and we would like to kindly ask you to help us and move the stuff into floor 0 (next to the reception, on the left side, next to the wall. If everyone grabs 1 bag, we already won).

tl&dr: Please help us to move stuff (plastic bags, cardboard boxes) from floor 3, IN FRONT OF THE ELEVATOR to floor 0 (next to the reception). Stuff beyond the toilets !MUST STAY!, only focus on the stuff in front of the elevators, in the corner.

Thank you in advance!

(Keyword: let's help the Schmuck ;) )