SCHEDULE:
15:30 Database reset, single-user Database (ONE ip/team)
16:00 Read-only database (no more registrations possible)
16:20 END OF CODING
16:30 Championship starts
	2 minutes OR 3 laps per race, 10 races
~17:00 Championship ends
~17:10 Results announced
