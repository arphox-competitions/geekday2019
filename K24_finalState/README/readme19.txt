EXACT bonuses obtainable with side tasks:

Top speed: original 10, with side task 13
Steering power: original 5, with side tasks 8 or 12
HP: original 10, with side tasks 13 or 16
Bullets: original 5, with side task 10