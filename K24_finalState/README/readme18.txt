SORRY, protocol bug discovered - there is a small change in the UDP data that you have to SEND:

Bug is: you can send numbers 0..20 for speed, and that stands for speed -10 .. +10. If you send anything outside [0..20], the packet is totally ignored. However, some cars WILL have a higher top speed (because of side tasks), so this validation is flawed.

OLD: you have to send speed 0..20, that means speed -10 .. +10

NEW: you have to send speed 0..30, that means speed -15 .. +15. If the speed is outside your [-topSpeed .. +topSpeed] interval, the speed change is ignored.
