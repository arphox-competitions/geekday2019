HTTP protocol change
- OLD: you only received HTTP messages about new bonuses
- NEW: you receive HTTP messages about bonuses picked up as well
	The two requests are:
		?bonus={newSpecial.C}&x={newSpecial.P.X}&y={newSpecial.P.Y}
		?bonusGONE={special.C}&x={special.P.X}&y={special.P.Y}