I'm going to sleep between 2am and 7am. You can ask questions until 2am :) 

If the server crashes, then feel free to rebuild + re-execute the server...

Try to wake me up if the building is on fire. Probably you can't, but at least try :) 

Adam will sleep too, but he is younger, so he'll sleep less.
Sidetasks that are sent to http://users.nik.uni-obuda.hu/ff/ (subject "KOCKANAP_GEEKDAY") until 5am will be checked, and their results will be uploaded around 7am.