BIG PROTOCOL CHANGE:

OLD: The first four bytes of every UDP packet sent from the server to the client was 0xFF
NEW: The first four bytes of every UDP packet sent from the server is a SEQUENCE ID (four times the same byte 0x00 - 0xFF)
		You can use this sequence ID to check which UDP packets belong to the same sequence!