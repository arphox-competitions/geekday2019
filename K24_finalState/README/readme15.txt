Gooooood morning, campers!

By now some of you might have a grasp of a semi-working solution. Maybe you can find the shortest road/best curve from checkpoint to checkpoint. Maybe you can indeed try and follow a path from checkpoint to checkpoint...

Now ask yourself, how crazy am I? How EVIL am I? ;) 

The specification never said that if you follow the curve of the track, checkpoint2 will be immediately after checkpoint1 (checkpoint 3 might be in between). The specification never said that there will be a road between checkpoint1 and checkpoint2 (non-road segments might be in between). 

Think of the current track. DID YOU PREPARE FOR SUCH PROBLEMS??? Trust me, you should ;) Okay, most of the tracks will be nice tracks like the current one, but some of them...

A) It is possible that checkpoints are mixed up. In this map: what if we replace the locations of checkpoint1 and checkpoint3 with each other?

B) It is possible that checkpoints might not be directly reachable: what if the goa'uld hits the stargate with an energy beam, and it explodes? What if it destroys the track around checkpoint2, so checkpoint2 will be in the middle of a non-track area? 

C) The road between the start line and checkpoint1 can be looooong if checkpoint0 is removed - maybe shortcuts CAN BE good???

Yes, I am that evil ;)

Good luck with today! If you don't want to prepare for these special cases, and you just create a car that can follow checkpoints in the correct order, it will still be a working and very good solution!!! 

But if you can, try and prepare for the crazy cases ;)
