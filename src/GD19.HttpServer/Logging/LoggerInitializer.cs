﻿using GD19.Common;
using Serilog;
using Serilog.Events;
using System;
using System.IO;

namespace GD19.HttpServer.Logging
{
    internal static class LoggerInitializer
    {
        private const string logFilesFolderBase = @"C:/_GEEKDAY/";
        private static readonly string currentRunFolder = DateTime.Now.ToSecondsTimestamp();
        private static readonly string logFilesFolder = Path.Combine(logFilesFolderBase, currentRunFolder);

        private const string normalLogFileName = @"log.txt";
        private const string errorLogFileName = @"log.error.txt";

        internal static void Initialize()
        {
            Log.Logger = new LoggerConfiguration()
                //.MinimumLevel.Verbose()
                .MinimumLevel.Debug()
                //.MinimumLevel.Information()
                //.MinimumLevel.Warning()
                //.MinimumLevel.Error()
                //.MinimumLevel.Fatal()
                .MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)
                .Enrich.FromLogContext()
                .AddFileLogger_General()
                .AddFileLogger_Error()
                //.WriteTo.Debug()   // Really slow, use only if absolutely needed!
                //.WriteTo.Console() // Really slow, use only if absolutely needed!
                .CreateLogger();
        }

        private static LoggerConfiguration AddFileLogger_General(this LoggerConfiguration lc)
        {
            return lc.WriteTo.File(
                path: Path.Combine(logFilesFolder, normalLogFileName),
                outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level:u3}] [{SourceContext}] {Message:lj}{NewLine}{Exception}",
                fileSizeLimitBytes: 1024 * 1024, // 1 MB
                buffered: true,
                flushToDiskInterval: TimeSpan.FromSeconds(1),
                rollOnFileSizeLimit: true);
        }

        private static LoggerConfiguration AddFileLogger_Error(this LoggerConfiguration lc)
        {
            return lc.WriteTo.File(
                restrictedToMinimumLevel: LogEventLevel.Error,
                path: Path.Combine(logFilesFolder, errorLogFileName));
        }
    }
}