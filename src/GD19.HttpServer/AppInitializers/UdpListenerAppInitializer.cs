﻿using GD19.Common.Settings;
using GD19.Networking;
using Serilog;
using System;

namespace GD19.HttpServer.AppInitializers
{
    public static class UdpListenerAppInitializer
    {
        public static void StartRunningInBackground(Action<byte[]> dataReceivedCallback)
        {
            int port = NetworkSettings.Udp.ListeningPort;
            ILogger logger = Log.Logger.ForContext<UdpListener>();

            var udpListener = new UdpListener(port, dataReceivedCallback, logger);
            udpListener.RunInBackground();
        }
    }
}