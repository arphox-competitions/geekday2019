﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Serilog;
using System.Threading.Tasks;

namespace GD19.HttpServer.AppInitializers
{
    public static class AspNetAppInitializer
    {
        public static Task RunAsync()
        {
            Log.Information("Starting ASP.NET app...");
            return CreateHostBuilder().Build().RunAsync();
        }

        public static IHostBuilder CreateHostBuilder()
        {
            return Host.CreateDefaultBuilder()
                .UseSerilog()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
        }
    }
}