﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace GD19.HttpServer.Controllers
{
    #region Test model objects
    public class GetTestEmployeeRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
    public class TestEmployee
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
    }
    #endregion

    [Route("[controller]")]
    [ApiController]
    public sealed class TestController : ControllerBase
    {
        [HttpGet]
        public string Index() => "Index";

        [HttpGet(nameof(Throw))]
        public string Throw() => throw new InvalidOperationException("Throwing test InvalidOperationException!");

        [HttpGet(nameof(GetEmployee))]
        public TestEmployee GetEmployee([FromQuery]int id)
        {
            return new TestEmployee()
            {
                ID = id,
                DateOfBirth = DateTime.Now.AddYears(-30),
                FirstName = "FirstName",
                LastName = "LastName"
            };
        }

        [HttpPost(nameof(PostEmployee))]
        public TestEmployee PostEmployee(GetTestEmployeeRequest request)
        {
            return new TestEmployee()
            {
                ID = 666,
                DateOfBirth = DateTime.Now.AddYears(-30),
                FirstName = request.FirstName,
                LastName = request.LastName
            };
        }
    }
}