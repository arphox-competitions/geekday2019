﻿using Serilog;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace GD19.Networking
{
    /// <summary>
    ///     Non thread-safe UdpListener
    /// </summary>
    /// <remarks>
    ///     Cancellation is not implemented because it wouldn't make sense since this object is not reusable.
    ///     So if you want to cancel, just dispose it.
    /// </remarks>
    public sealed class UdpListener : IDisposable
    {
        private readonly int _port;
        private readonly Action<byte[]> _dataReceivedCallback;
        private readonly ILogger _logger;
        private UdpClient _listener;

        public bool IsDisposed { get; private set; } = false;
        public bool IsRunning { get; private set; } = false;

        public UdpListener(int port, Action<byte[]> dataReceivedCallback, ILogger logger)
        {
            _port = port;
            _dataReceivedCallback = dataReceivedCallback ?? throw new ArgumentNullException(nameof(dataReceivedCallback));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public void RunInBackground()
        {
            _logger.Verbose($"{nameof(RunInBackground)} has been called.");
            PrepareRun();
            Task.Factory.StartNew(
                () => EndlessReceive(),
                TaskCreationOptions.LongRunning);
        }

        public void RunSynchronously()
        {
            _logger.Verbose($"{nameof(RunSynchronously)} has been called.");
            PrepareRun();
            EndlessReceive();
        }

        private void EndlessReceive()
        {
            while (true)
            {
                if (IsDisposed)
                {
                    _logger.Information(@"Stopping next receive cycle as we are disposed.");
                    return;
                }

                ReceiveOnce();
            }
        }

        private void ReceiveOnce()
        {
            byte[] data;
            try
            {
                IPEndPoint remoteEndpoint = null; // we don't need this
                _logger.Debug("Starting to wait for incoming UDP data...");
                data = _listener.Receive(ref remoteEndpoint);
                _logger.Debug("Received UDP data. As UTF8 string: \"{data}\"", Encoding.UTF8.GetString(data));
            }
            catch (SocketException e) when (e.ErrorCode == 10004)
            {
                // 10004: "Interrupted function call. A blocking operation was interrupted by a call to WSACancelBlockingCall."
                // -> this means we have been cancelled
                _logger.Warning("Receive interrupted by a WSACancelBlockingCall.");
                return;
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error occured while receiving UDP.");
                return;
            }

            _logger.Debug($"Calling {nameof(_dataReceivedCallback)}...");
            _dataReceivedCallback(data);
            _logger.Debug($"Finished {nameof(_dataReceivedCallback)}...");
        }

        private void PrepareRun()
        {
            if (IsDisposed)
                throw new ObjectDisposedException(nameof(UdpListener));

            if (IsRunning)
                throw new InvalidOperationException("The listener is already running.");

            IsRunning = true;
            _listener = new UdpClient(_port);
        }

        public void Stop()
        {
            _logger.Information($"{nameof(Stop)} has been called.");
            Dispose();
        }

        public void Dispose()
        {
            if (IsDisposed)
                return;

            _listener?.Close();
            _listener?.Dispose();
            _listener = null;
            IsRunning = false;
            IsDisposed = true;
        }
    }
}