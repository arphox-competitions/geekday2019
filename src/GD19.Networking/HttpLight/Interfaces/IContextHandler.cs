﻿using System.Net;

namespace GD19.Networking.HttpLight.Interfaces
{
    public interface IContextHandler
    {
        void Handle(HttpListenerContext context);
    }
}