﻿using System;
using System.Text;

namespace GD19.Networking.HttpLight.Model
{
    public sealed class EmptySuccessResponse : LightHttpResponse
    {
        public EmptySuccessResponse()
            : base(200, "OK", Array.Empty<byte>(), Encoding.UTF8, "text/plain")
        {
        }
    }
}