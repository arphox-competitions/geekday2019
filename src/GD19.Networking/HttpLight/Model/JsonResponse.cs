﻿using System.Text;

namespace GD19.Networking.HttpLight.Model
{
    public sealed class JsonResponse : LightHttpResponse
    {
        public JsonResponse(
            int statusCode,
            string statusDescription,
            byte[] bodyBytes,
            Encoding contentEncoding)
            : base(statusCode, statusDescription, bodyBytes, contentEncoding, "application/json")
        { }

        public static JsonResponse CreateUtf8Success(string json)
        {
            return new JsonResponse(200, "OK", Encoding.UTF8.GetBytes(json), Encoding.UTF8);
        }
    }
}