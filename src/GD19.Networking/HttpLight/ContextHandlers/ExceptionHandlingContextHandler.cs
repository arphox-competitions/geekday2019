﻿using GD19.Networking.HttpLight.Interfaces;
using Serilog;
using System;
using System.Net;

namespace GD19.Networking.HttpLight
{
    public sealed class ExceptionHandlingContextHandler : IContextHandler
    {
        private readonly IContextHandler _next;
        private readonly ILogger _logger;

        public ExceptionHandlingContextHandler(IContextHandler next, ILogger logger)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public void Handle(HttpListenerContext context)
        {
            try
            {
                _logger.Verbose($"Proceeding through {nameof(ExceptionHandlingContextHandler)}...");
                _next.Handle(context);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Exception thrown while handling a HTTP request.");
                throw;
            }
        }
    }
}