﻿using GD19.Common.Settings;
using GD19.GameLogic;
using System;
using System.Net.Sockets;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace GD19.Visualizer
{
    public partial class ManualControlWindow : Window
    {
        private DispatcherTimer _controlTimer = new DispatcherTimer();
        private UdpClient senderClient;
        private CarController _carController;


        public ManualControlWindow()
        {
            InitializeComponent();

            senderClient = new UdpClient();
            senderClient.Connect(NetworkSettings.IPs.ServerIP, NetworkSettings.Udp.ServerPort);
            
            _carController = new CarController(senderClient);
            InitializeTimer();
        }

        public ManualControlWindow(CarController carController)
        {
            InitializeComponent();
            _carController = carController;
            InitializeTimer();
        }

        private void InitializeTimer()
        {
            _controlTimer.Interval = TimeSpan.FromMilliseconds(50);
            _controlTimer.Tick += _controlTimer_Tick;
            _controlTimer.Start();
        }

        private void _controlTimer_Tick(object sender, EventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.W)) _carController.TryIncreaseSpeed();
            if (Keyboard.IsKeyDown(Key.E)) _carController.SetMaxSpeed();

            if (Keyboard.IsKeyDown(Key.S)) _carController.TryDecreaseSpeed();
            if (Keyboard.IsKeyDown(Key.Q)) _carController.SetMinSpeed();

            if (Keyboard.IsKeyDown(Key.A)) _carController.TurnLeft();
            if (Keyboard.IsKeyDown(Key.D)) _carController.TurnRight();
            if (Keyboard.IsKeyDown(Key.F)) _carController.Stop();

            if (Keyboard.IsKeyDown(Key.Space)) _carController.TryShootRocket();
            if (Keyboard.IsKeyDown(Key.Tab)) _carController.TryPutMine();
        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.D0: PatternZero(); break;
                case Key.D1: PatternOne(); break;
            }
        }

        private void PatternZero()
        {
            _carController.Stop();
            _carController.Send(CarController.MY_MAX_SPEED, 0);
            Thread.Sleep(1000);

            _carController.Stop();
            _carController.Send(CarController.MY_MAX_SPEED, 9);
            Thread.Sleep(1000);

            _carController.Stop();
            _carController.Send(CarController.MY_MAX_SPEED, 18);
            Thread.Sleep(1000);

            _carController.Stop();
            _carController.Send(CarController.MY_MAX_SPEED, 27);
            Thread.Sleep(1000);

            _carController.Stop();
            Thread.Sleep(2000);
        }

        private void PatternOne()
        {
            for (int i = 0; i < 5; i++)
            {
                _carController.Stop();
                _carController.SetAngle(0);
                Thread.Sleep(1200);
                _carController.SetMaxSpeed();
                Thread.Sleep(1000);

                _carController.Stop();
                _carController.SetAngle(27);
                Thread.Sleep(1200);
                _carController.SetMaxSpeed();
                Thread.Sleep(1000);

                _carController.Stop();
                _carController.SetAngle(18);
                Thread.Sleep(1200);
                _carController.SetMaxSpeed();
                Thread.Sleep(1000);

                _carController.Stop();
                _carController.SetAngle(9);
                Thread.Sleep(1200);
                _carController.SetMaxSpeed();
                Thread.Sleep(1000);

                _carController.Stop();
            }
        }
    }
}