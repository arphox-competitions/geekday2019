﻿using GD19.GameLogic;
using System;
using System.Diagnostics;
using System.Threading;
using System.Windows;

namespace GD19.Visualizer
{
    public static class AppInitializer
    {
        private static App currentApp = null;

        public static void RunMapVisualizer(AI ai)
        {
            Run(() => new MapVisualizerWindow(ai));
        }

        public static void RunWithCustomCarController(CarController carController)
        {
            Run(() => new ManualControlWindow(carController));
        }

        private static void Run(Func<Window> getWindow)
        {
            if (currentApp != null)
            {
                Debug.WriteLine(">>>> New race started but no Visualizer window for that! <<<<");
                return;
            }

            Thread wpfThread = new Thread(() =>
            {
                currentApp = new App
                {
                    ShutdownMode = ShutdownMode.OnExplicitShutdown
                };
                currentApp.Run(getWindow());

            });

            wpfThread.SetApartmentState(ApartmentState.STA);
            wpfThread.IsBackground = true;
            wpfThread.Name = "WpfVisualizer_Host";
            wpfThread.Start();
        }
    }
}