﻿using Serilog;
using System;
using System.Windows;

namespace GD19.Visualizer
{
    public partial class MainWindow : Window
    {
        public MainWindow()
            : this(new LoggerConfiguration().CreateLogger())
        {
        }

        public MainWindow(ILogger logger)
        {
            if (logger is null) throw new ArgumentNullException(nameof(logger));
            logger.Information($"Initializing WPF App ({nameof(MainWindow)})");
            InitializeComponent();

            new ManualControlWindow().Show();
            this.Close();
        }
    }
}