﻿using GD19.Common.Settings;
using GD19.GameLogic;
using GD19.GameLogic.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace GD19.Visualizer
{
    public partial class MapVisualizerWindow : Window
    {
        private static Random random = new Random();

        private readonly AI _ai;
        private DispatcherTimer _renderTimer = new DispatcherTimer();
        private int[,] drawMask = null;

        public MapVisualizerWindow(AI ai)
        {
            _ai = ai ?? throw new ArgumentNullException(nameof(ai));

            InitializeComponent();
            _renderTimer.Interval = TimeSpan.FromMilliseconds(50);
            _renderTimer.Tick += RenderTimer_Tick;

            while (!_ai.GameMap.IsInitialized) // wait until initialization is done
                Thread.Sleep(10);

            Stopwatch stopper = Stopwatch.StartNew();
            SetDrawMask();
            DrawMap();
            Debug.WriteLine($"Drawed initial map in {stopper.ElapsedMilliseconds} ms.");

            _renderTimer.Start();
        }

        private void RenderTimer_Tick(object sender, EventArgs e)
        {
            SetDrawMask();

            Dictionary<(int x, int y), int> dict = new Dictionary<(int x, int y), int>();
            dict.Add((1, 0), IranyVonalBeallitas(1, 0)); // előre
            dict.Add((1, 1), IranyVonalBeallitas(1, 1));  // jobbra 45
            dict.Add((1, -1), IranyVonalBeallitas(1, -1)); // balra 45
            dict.Add((0, 1), IranyVonalBeallitas(0, 1)); // jobbra
            dict.Add((0, -1), IranyVonalBeallitas(0, -1)); // balra

            byte newAngle = 0;
            int maxValue = dict.Max(kvp => kvp.Value);
            (int x, int y) erreALegtavolabb = dict.Where(d => d.Value == maxValue).First().Key;

            switch (erreALegtavolabb)
            {
                case (1, 0): newAngle = 0; break;
                case (1, 1): newAngle = 4; break;
                case (0, 1): newAngle = 9; break;
                case (-1, 1): newAngle = 13; break;
                case (-1, 0): newAngle = 18; break;
                case (-1, -1): newAngle = 23; break;
                case (0, -1): newAngle = 27; break;
                case (1, -1): newAngle = 31; break;
            }

            byte myAngle = _ai.GameMap.OurCar.Angle;
            newAngle = (byte)((myAngle + newAngle) % 36);

            if (maxValue < 5)
            {
                _ai.CarController.SetMinSpeed();
                _ai.CarController.DisableForTimespan(TimeSpan.FromMilliseconds(130));
            }

            _ai.CarController.Stop();
            _ai.CarController.SetAngle(newAngle);
            Debug.WriteLine($"New angle sent: {newAngle} \tMaxvalue:{maxValue}");
            //_ai.CarController.SetSpeed(17);
        
            _ai.CarController.SetSpeed(maxValue > 40 ? (byte)26 : (byte)23);

            if (random.Next(0, 100) < 5)
                _ai.CarController.TryPutMine();
            if (random.Next(0, 100) < 5)
                _ai.CarController.TryShootRocket();

            DrawMap();
        }

        private int IranyVonalBeallitas(int dirX, int dirY)
        {
            try
            {
                Car car = _ai.GameMap.OurCar;
                double currentX = car.X;
                double currentY = car.Y;

                (double iranyX, double iranyY) = MathHelper.GetElforgatottIranyvektor(car.Angle, dirX, dirY);

                int value = drawMask[car.X, car.Y];
                int distance = 0;

                currentX += 2 * iranyX;
                currentY += 2 * iranyY;

                while (value > 1000 && value < 9000)
                {
                    currentX += 4 * iranyX;
                    currentY += 4 * iranyY;
                    value = drawMask[(int)currentX, (int)currentY];
                    drawMask[(int)currentX, (int)currentY] = 9001;
                    distance++;
                }

                return distance;
            }
            catch
            {
                return 0;
            }
        }

        private void DrawMap()
        {
            if (drawMask == null)
                return;

            int maxValue = drawMask.Cast<int>().Where(c => c < 2000).Max();

            int width = drawMask.GetLength(0);
            int height = drawMask.GetLength(1);

            WriteableBitmap wbitmap = new WriteableBitmap(width, height, 96, 96, PixelFormats.Bgra32, null); // B G R A
            byte[,,] pixels = new byte[height, width, 4];

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    int val = drawMask[x, y];
                    if (val == 1)
                    {   // white
                        pixels[y, x, 0] = 255; pixels[y, x, 1] = 255; pixels[y, x, 2] = 255; pixels[y, x, 3] = 255;
                    }
                    else if (val == 0)
                    {
                        // black
                        pixels[y, x, 0] = 0; pixels[y, x, 1] = 0; pixels[y, x, 2] = 0; pixels[y, x, 3] = 255;
                    }
                    else if (val == 9001)
                    {
                        // white
                        pixels[y, x, 0] = 255; pixels[y, x, 1] = 255; pixels[y, x, 2] = 255; pixels[y, x, 3] = 255;
                    }
                    else if (val == maxValue)
                    {
                        // dark red
                        pixels[y, x, 0] = 0; pixels[y, x, 1] = 0; pixels[y, x, 2] = 220; pixels[y, x, 3] = 255;
                    }
                    else if (val >= 1000)
                    {
                        val -= 1000;
                        byte c = (byte)(val * (255 / 100.0));
                        pixels[y, x, 0] = c; pixels[y, x, 1] = c; pixels[y, x, 2] = c; pixels[y, x, 3] = 255;
                    }
                }
            }


            // Copy the data into a one-dimensional array.
            byte[] pixels1d = new byte[height * width * 4];
            int index = 0;
            for (int row = 0; row < height; row++)
            {
                for (int col = 0; col < width; col++)
                {
                    for (int i = 0; i < 4; i++)
                        pixels1d[index++] = pixels[row, col, i];
                }
            }

            Int32Rect rect = new Int32Rect(0, 0, width, height);
            int stride = 4 * width;
            wbitmap.WritePixels(rect, pixels1d, stride, 0);

            Image img = new Image();
            img.Stretch = Stretch.None;
            img.Margin = new Thickness(0);

            maingrid.Children.Clear();
            maingrid.Children.Add(img);

            img.Source = wbitmap;
        }

        private void SetDrawMask()
        {
            // copy 2D array
            drawMask = new int[_ai.GameMap.LevelMask.GetLength(0), _ai.GameMap.LevelMask.GetLength(1)];
            Array.Copy(_ai.GameMap.LevelMask, 0, drawMask, 0, _ai.GameMap.LevelMask.Length);
        }
    }
}