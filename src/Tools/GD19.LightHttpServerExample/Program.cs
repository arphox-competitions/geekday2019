﻿using GD19.Networking.HttpLight;
using GD19.Networking.HttpLight.ContextHandlers;
using GD19.Networking.HttpLight.Model;
using Serilog;
using System;

namespace GD19.LightHttpServerExample
{
    class Program
    {
        static void Main()
        {
            ILogger fileLogger = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .WriteTo.File(
                    path: @"C:\temp\log.txt",
                    outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level:u3}] [{SourceContext}] {Message:lj}{NewLine}{Exception}")
                .CreateLogger();

            ILogger consoleLogger = new LoggerConfiguration()
                .MinimumLevel.Information()
                .WriteTo.Console(
                    outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level:u3}] [{SourceContext}] {Message:lj}{NewLine}{Exception}")
                .CreateLogger();

            Func<LightHttpRequest, LightHttpResponse> responderMethod = _ =>
            {
                string message = $"Hello world! at {DateTime.Now.ToString("o")}";
                return TextResponse.CreateUtf8Success(message);
            };

            using var httpServerLight = new LightweightHttpServerBuilder()
                .DisableConcurrentRequestServing()
                .WithLogger(fileLogger.ForContext<LightweightHttpServer>())
                .Add<ResponseTimeMeasurerContextHandler>(consoleLogger) // only gets logged to the console
                .Add<ExceptionHandlingContextHandler>()
                //.Add<IpBlacklistContextHandler>(new object[] { new string[] { "192.168.1.199" } })
                .Add<IpWhitelistContextHandler>(new object[] { Array.Empty<string>() })
                .Add<BusinessLogicContextHandler>(responderMethod)
                .Build();

            httpServerLight.Start();

            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }
    }
}