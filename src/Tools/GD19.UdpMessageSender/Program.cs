﻿using GD19.Common.Settings;
using System;
using System.Diagnostics;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace GD19.UdpMessageSender
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("This is the UDP sender.");
            Console.ResetColor();

            const string hostname = "localhost";
            int port = NetworkSettings.Udp.ListeningPort;

            using UdpClient sender = new UdpClient();
            sender.Connect(hostname, port);

            SendMessages(sender);
        }

        private static void SendMessages(UdpClient sender)
        {
            try
            {
                TrySendMessages(sender);
            }
            catch (SocketException e)
            {
                Debugger.Break();
                Console.WriteLine(e);
            }
            finally
            {
                sender.Close();
            }
        }

        private static void TrySendMessages(UdpClient sender)
        {
            int counter = 0;

            while (true)
            {
                string message = $"Number {counter} at {DateTime.Now.ToString()}";
                byte[] bytes = Encoding.UTF8.GetBytes(message);

                Console.Write($"Sending '{message}'...\t");
                sender.Send(bytes, bytes.Length);
                Console.WriteLine("Sent!");

                counter++;
                Thread.Sleep(200);
            }
        }
    }
}