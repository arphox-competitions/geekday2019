﻿using CarService;
using GD19.SoapProject.Model;
using Serilog;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GD19.SoapProject
{
    public sealed class CheckpointRetriever
    {
        public static async Task<Checkpoint[]> GetCheckpointsAsync(ILogger logger, string trackId)
        {
            string checkpointsString = await GetRawCheckpointsAsync(logger, trackId).ConfigureAwait(false);

            XDocument xdoc = XDocument.Parse(checkpointsString);
            return xdoc.Root
                .Elements()
                .Select(e =>
                {
                    int x1 = int.Parse(e.Attribute("x1").Value);
                    int y1 = int.Parse(e.Attribute("y1").Value);

                    int x2 = int.Parse(e.Attribute("x2").Value);
                    int y2 = int.Parse(e.Attribute("y2").Value);

                    return new Checkpoint(x1, y1, x2, y2);
                })
                .ToArray();
        }

        public static async Task<string> GetRawCheckpointsAsync(ILogger logger, string trackId)
        {
            if (logger is null) throw new ArgumentNullException(nameof(logger));
            if (trackId is null) throw new ArgumentNullException(nameof(trackId));

            CarServiceClient client = null;
            string checkPoints = null;
            try
            {
                client = new CarServiceClient();
                logger.Debug("Starting to download checkpoint data for track '{0}'...", trackId);
                checkPoints = await client.GetCheckpointsAsync(trackId).ConfigureAwait(false);
                logger.Debug("Checkpoint data for track '{0}'", checkPoints);
            }
            finally
            {
                if (client != null)
                    await client.CloseAsync().ConfigureAwait(false);
            }

            return checkPoints;
        }
    }
}