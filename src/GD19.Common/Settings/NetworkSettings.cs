﻿namespace GD19.Common.Settings
{
    public static class NetworkSettings
    {
        public static class Udp
        {
            public const int ListeningPort = 55555;
            public const int ServerPort = 9999;
        }

        public static class IPs
        {
            public const string ServerIP = "192.168.1.20";
        }
    }
}