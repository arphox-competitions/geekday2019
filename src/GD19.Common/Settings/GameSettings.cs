﻿namespace GD19.Common.Settings
{
    public static class GameSettings
    {
        public const int OurPlayerId = 0;
        public const int RocketSpeed = 15;
        public const int CarLength = 40;
        public const int CarWidth = 20;
    }
}