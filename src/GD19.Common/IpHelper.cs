﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace GD19.Common
{
    public static class IpHelper
    {
        public static string GetLocalIPv4Address()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());

            string ip = host.AddressList
                .FirstOrDefault(a => a.AddressFamily == AddressFamily.InterNetwork)
                ?.ToString();

            if (ip == null)
                throw new Exception("No network adapters with an IPv4 address in the system!");

            return ip;
        }
    }
}