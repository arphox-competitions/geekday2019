﻿using FluentAssertions;
using GD19.GameLogic.NetworkHandlers.UdpInput;
using Xunit;

namespace GD19.IntegrationTests
{
    public sealed class SequenceDataTests
    {
        [Fact]
        public void GoodOrder_3Elements()
        {
            SequenceData sut = new SequenceData(3);
            sut.IsReady().Should().BeFalse();

            sut.AcceptPacket(0, new byte[] { 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5 }).Should().BeNullOrEmpty();
            sut.IsReady().Should().BeFalse();

            sut.AcceptPacket(1, new byte[] { 0, 0, 0, 0, 0, 0, 6, 7 }).Should().BeNullOrEmpty();
            sut.IsReady().Should().BeFalse();

            byte[] result = sut.AcceptPacket(2, new byte[] { 0, 0, 0, 0, 0, 0, 8, 9, 10, 11 });
            sut.IsReady().Should().BeTrue();
            result.Should().NotBeEmpty();
            result.Should().HaveCount(11);
            result.Should().BeEquivalentTo(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
        }

        [Fact]
        public void BadOrder1_3Elements()
        {
            SequenceData sut = new SequenceData(3);
            sut.IsReady().Should().BeFalse();

            sut.AcceptPacket(1, new byte[] { 0, 0, 0, 0, 0, 0, 6, 7 }).Should().BeNullOrEmpty();
            sut.IsReady().Should().BeFalse();

            sut.AcceptPacket(0, new byte[] { 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5 }).Should().BeNullOrEmpty();
            sut.IsReady().Should().BeFalse();

            byte[] result = sut.AcceptPacket(2, new byte[] { 0, 0, 0, 0, 0, 0, 8, 9, 10, 11 });
            sut.IsReady().Should().BeTrue();
            result.Should().NotBeEmpty();
            result.Should().HaveCount(11);
            result.Should().BeEquivalentTo(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
        }

        [Fact]
        public void BadOrder2_3Elements()
        {
            SequenceData sut = new SequenceData(3);
            sut.IsReady().Should().BeFalse();

            sut.AcceptPacket(1, new byte[] { 0, 0, 0, 0, 0, 0, 6, 7 }).Should().BeNullOrEmpty();
            sut.IsReady().Should().BeFalse();

            sut.AcceptPacket(2, new byte[] { 0, 0, 0, 0, 0, 0, 8, 9, 10, 11 }).Should().BeNullOrEmpty();
            sut.IsReady().Should().BeFalse();

            byte[] result = sut.AcceptPacket(0, new byte[] { 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5 });
            sut.IsReady().Should().BeTrue();

            result.Should().NotBeEmpty();
            result.Should().HaveCount(11);
            result.Should().BeEquivalentTo(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
        }
    }
}