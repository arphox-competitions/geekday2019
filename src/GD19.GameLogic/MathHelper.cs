﻿using System;

namespace GD19.GameLogic
{
    public static class MathHelper
    {
        public static (double x, double y) GetElforgatottIranyvektor(int carAngle, int dirX, int dirY)
        {
            double carAngleRad = ToRadian(carAngle * 10);

            double cosA = Math.Cos(carAngleRad);
            double sinA = Math.Sin(carAngleRad);

            double xDash = dirX * cosA - dirY * sinA;
            double yDash = dirX * sinA + dirY * cosA;

            return (xDash, yDash);
        }

        private static double ToRadian(int angle) => angle * Math.PI / 180;
    }
}