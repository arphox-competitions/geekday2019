﻿namespace GD19.GameLogic
{
    public sealed class RaceContext
    {
        public int PlayerId { get; set; }
        public string TrackId { get; set; }
        public bool IsStarted { get; set; }
    }
}