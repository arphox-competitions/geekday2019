﻿using GD19.Common.Settings;
using GD19.SoapProject.Model;
using Serilog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace GD19.GameLogic.Model
{
    public sealed class GameMap
    {
        private GameObjectContainer _gameObjects;
        public GameObjectContainer GameObjects
        {
            get => _gameObjects;
            set
            {
                _gameObjects = value;
                OurCar = FindOurCar();
                OverrideOurCarSpeedAndAngle();
            }
        }

        public Car OurCar { get; private set; }
        public int[,] LevelMask { get; set; }
        private readonly ILogger _logger;
        private readonly AI _parentAi;

        public ConcurrentBag<Bonus> Bonuses { get; } = new ConcurrentBag<Bonus>();
        public bool IsInitialized { get; set; }
        public Checkpoint[] Checkpoints { get; set; }

        public GameMap(ILogger logger, AI parentAi)
        {
            _logger = logger ?? throw new System.ArgumentNullException(nameof(logger));
            _parentAi = parentAi ?? throw new ArgumentNullException(nameof(parentAi));
        }

        public void AddBonus(Bonus bonus)
        {
            Bonuses.Add(bonus);
            _logger.Information("New bonus added: '{B}'", bonus.ToString());
        }

        public void RemoveBonus(char bonusType, int x, int y)
        {
            Bonus bonus = Bonuses.FirstOrDefault(b =>
                b.X == x &&
                b.Y == y &&
                b.BonusType == bonusType);

            if (Bonuses.TryTake(out bonus))
                _logger.Information("Removed bonus: '{B}'", bonus.ToString());
        }

        public void InitializeOurMap()
        {
            InitializeEdge();
        }

        private void InitializeEdge()
        {
            Extend(0, 1000); // Edge
            int searchValue = 1000;
            for (int i = 0; i < 50; i++)
            {
                Extend(searchValue, searchValue + 1);
                searchValue++;
            }
        }

        private void Extend(int searchValue, int valueToSet)
        {
            int xMax = LevelMask.GetLength(0) - 1;
            int yMax = LevelMask.GetLength(1) - 1;

            for (int x = 1; x < xMax; x++)
            {
                for (int y = 1; y < yMax; y++)
                {
                    if (LevelMask[x, y] == 1 &&
                        (
                            LevelMask[x - 1, y] == searchValue || // Left
                            LevelMask[x - 1, y - 1] == searchValue || // LeftUp
                            LevelMask[x, y - 1] == searchValue || //Up
                            LevelMask[x + 1, y - 1] == searchValue || //UpRight
                            LevelMask[x + 1, y] == searchValue || //Right
                            LevelMask[x + 1, y + 1] == searchValue || // RightDown
                            LevelMask[x, y + 1] == searchValue || // Down
                            LevelMask[x - 1, y + 1] == searchValue // DownLeft
                        ))
                    {
                        LevelMask[x, y] = Math.Min(valueToSet, 1020);
                    }
                }
            }
        }

        private Car FindOurCar()
        {
            foreach (KeyValuePair<(int, int), List<GameObject>> entry in GameObjects.Items)
            {
                foreach (GameObject currGameObj in entry.Value)
                {
                    if (currGameObj.PlayerId == GameSettings.OurPlayerId &&
                        currGameObj is Car c)
                    {
                        return c;
                    }
                }
            }

            return null;
        }

        private void OverrideOurCarSpeedAndAngle()
        {
            _parentAi.CarController.OverrideCurrentAngle(OurCar.Angle);
            _parentAi.CarController.OverrideCurrentSpeed(OurCar.Speed);
        }
    }
}