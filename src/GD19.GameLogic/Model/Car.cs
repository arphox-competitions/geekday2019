﻿namespace GD19.GameLogic.Model
{
    public sealed class Car : GameObject
    {
        public byte NextCheckPointIndex { get; }
        public byte Hp { get; }
        public byte MineCount { get; }
        public byte RocketCount { get; }
        public byte Angle { get; }
        public byte DesiredAngle { get; }

        public Car(int x, int y, int? playerId, byte speed, byte nextCheckPointId,
            byte hp, byte mineCount, byte rocketCount, byte angle, byte desiredAngle)
            : base(x, y, playerId, speed)
        {
            NextCheckPointIndex = nextCheckPointId;
            Hp = hp;
            MineCount = mineCount;
            RocketCount = rocketCount;
            Angle = angle;
            DesiredAngle = desiredAngle;
        }

        public override string ToString() => $"[CAR@{X},{Y}] pid: {PlayerId}, HP: {Hp}, ANG: {Angle}, SP: {Speed}";
    }
}