﻿namespace GD19.GameLogic.Model
{
    public sealed class Bonus : GameObject
    {
        public char BonusType { get; }

        public Bonus(char bonusType, int x, int y)
            : base(x, y, null, 0)
        {
            BonusType = bonusType;
        }

        public override string ToString() => $"[Bonus {BonusType} @{X},{Y}]";
    }
}