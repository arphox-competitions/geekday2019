﻿namespace GD19.GameLogic.Model
{
    public sealed class Bomb : GameObject
    {
        public Bomb(int x, int y, int? playerId, byte speed)
            : base(x, y, playerId, speed)
        { }

        public override string ToString() => $"[Bomb @{X},{Y}] pid: {PlayerId}";
    }
}