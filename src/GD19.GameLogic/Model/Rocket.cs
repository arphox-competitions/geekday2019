﻿namespace GD19.GameLogic.Model
{
    public sealed class Rocket : GameObject
    {
        public Rocket(int x, int y, int? playerId, byte speed)
            : base(x, y, playerId, speed)
        { }

        public override string ToString() => $"[Rocket @{X},{Y}] pid: {PlayerId}, SPD: {Speed}";
    }
}