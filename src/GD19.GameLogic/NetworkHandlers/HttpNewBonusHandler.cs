﻿using GD19.GameLogic.Model;
using GD19.Networking.HttpLight.Model;
using System.Linq;

namespace GD19.GameLogic.NetworkHandlers
{
    public static class HttpNewBonusHandler
    {
        public static bool CanApply(LightHttpRequest request)
        {
            return 
                request.QueryString.Count > 0 &&
                request.QueryString["bonus"] != null &&
                request.QueryString["x"] != null &&
                request.QueryString["y"] != null;
        }

        public static Bonus Handle(LightHttpRequest request)
        {
            char bonusTypeChar = request.QueryString.Get("bonus").Single();
            int x = int.Parse(request.QueryString.Get("x"));
            int y = int.Parse(request.QueryString.Get("y"));

            return new Bonus(bonusTypeChar, x, y);
        }
    }
}