﻿using GD19.Networking.HttpLight.Model;
using System.Linq;

namespace GD19.GameLogic.NetworkHandlers
{
    public static class HttpBonusGoneHandler
    {
        public static bool CanApply(LightHttpRequest request)
        {
            return
                request.QueryString.Count > 0 &&
                request.QueryString["bonusGONE"] != null &&
                request.QueryString["x"] != null &&
                request.QueryString["y"] != null;
        }

        public static void Handle(LightHttpRequest request, Model.GameMap gameMap)
        {
            char bonusTypeChar = request.QueryString.Get("bonusGONE").Single();
            int x = int.Parse(request.QueryString.Get("x"));
            int y = int.Parse(request.QueryString.Get("y"));

            gameMap.RemoveBonus(bonusTypeChar, x, y);
        }
    }
}