﻿using System;
using System.Linq;

namespace GD19.GameLogic.NetworkHandlers.UdpInput
{
    public sealed class SequenceData
    {
        private readonly int _packetCount;
        private byte[][] _fragments;
        public int Age { get; set; }
        public int ExpireAge { get; }

        public SequenceData(int packetCount)
        {
            _packetCount = packetCount;
            _fragments = new byte[_packetCount][];
            ExpireAge = Math.Max(_packetCount + 10, 10);
        }

        /// <summary>
        ///     Processes the packet, and IF that was the last missing fragment, returns the full sequence data
        /// </summary>
        public byte[] AcceptPacket(int packetSerialNumber, byte[] fullInput)
        {
            if (_fragments[packetSerialNumber] != null)
                throw new ArgumentException("This fragment is already processed!");

            _fragments[packetSerialNumber] = fullInput[6..];
            if (IsReady())
            {
                byte[] fullData = CreateFullData();
                return fullData;
            }
            else
            {
                return null;
            }
        }

        public bool IsReady() => _fragments.All(f => f != null);

        private byte[] CreateFullData()
        {
            byte[] fullData = new byte[GetFullSize()];
            int fullIndex = 0;

            for (int i = 0; i < _fragments.Length; i++)
            {
                _fragments[i].CopyTo(fullData, fullIndex);
                fullIndex += _fragments[i].Length;
            }

            return fullData;
        }

        private int GetFullSize()
        {
            int size = 0;

            for (int i = 0; i < _fragments.Length; i++)
                size += _fragments[i].Length;

            return size;
        }
    }
}