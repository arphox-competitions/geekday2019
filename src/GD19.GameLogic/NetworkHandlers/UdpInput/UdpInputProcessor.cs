﻿using GD19.Common.Settings;
using GD19.GameLogic.Model;
using GD19.GameLogic.NetworkHandlers.UdpInput;
using GD19.Networking;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace GD19.GameLogic.UdpInput
{
    public sealed class UdpInputProcessor : IDisposable
    {
        private readonly ILogger _logger;
        private readonly AI _ai;
        private UdpListener _udpListener;
        private SequenceData[] sequenceDatas = new SequenceData[256];

        public UdpInputProcessor(ILogger logger, AI ai)
        {
            _logger = logger;
            _ai = ai ?? throw new ArgumentNullException(nameof(ai));
        }

        public void StartListening()
        {
            _udpListener = new UdpListener(
                NetworkSettings.Udp.ListeningPort,
                ProcessDataPack,
                _logger.ForContext<UdpListener>());

            _udpListener.RunInBackground();
        }

        public void ProcessDataPack(byte[] input)
        {
            Console.Write('.');
            int sequenceNumber = input[0]; // 0 = 1 = 2 = 3
            CheckSequenceNumbersIntegrity(input);
            int packetCount = input[4];
            int currentPacketSerialNumber = input[5];

            string message = string.Join(", ", input.Select(b => b.ToString()));
            _logger.Debug("UDP data pack received: '{0}'", message);

            ProcessDataPack(sequenceNumber, packetCount, currentPacketSerialNumber, input);
        }

        private static void CheckSequenceNumbersIntegrity(byte[] input)
        {
            int first = input[0];
            if (input[1] != first || input[2] != first || input[3] != first)
            {
                Debugger.Break(); // Nem ugyanaz az első 4 byte!
            }
        }

        private void ProcessDataPack(int sequenceNumber, int packetCount, int currentPacketSerialNumber, byte[] fullInput)
        {
            WipeExpiredSequenceDatas();

            if (sequenceDatas[sequenceNumber] == null)
                sequenceDatas[sequenceNumber] = new SequenceData(packetCount);

            byte[] result = sequenceDatas[sequenceNumber].AcceptPacket(currentPacketSerialNumber, fullInput);
            if (sequenceDatas[sequenceNumber].IsReady())
            {
                _logger.Debug("Data for sequenceNumber {sq} is ready and is {len} long.", sequenceNumber, result.Length);
                ProcessFullSequence(result);
                sequenceDatas[sequenceNumber] = null;
            }
        }

        private void ProcessFullSequence(byte[] result)
        {
            Dictionary<(int, int), List<GameObject>> dict = new Dictionary<(int, int), List<GameObject>>();

            int i = 0;
            while (i < result.Length)
            {
                switch (result[i])
                {
                    case 1:  // CAR
                        Car car = ReadCar(result[i..(i + 13)]);
                        AddItemToDictionary((car.X, car.Y), car);
                        i += 13;
                        break;
                    case 2: // ROCKET
                        Rocket rocket = ReadRocket(result[i..(i + 7)]);
                        AddItemToDictionary((rocket.X, rocket.Y), rocket);
                        i += 7;
                        break;
                    case 3: // BOMB
                        Bomb bomb = ReadBomb(result[i..(i + 7)]);
                        AddItemToDictionary((bomb.X, bomb.Y), bomb);
                        i += 7;
                        break;
                    default:
                        throw new ArgumentException($"Invalid object category '{result[i]}'");
                }
            }

            _ai.GameMap.GameObjects = new GameObjectContainer(dict);

            // -----------------
            void AddItemToDictionary((int, int) key, GameObject value)
            {
                if (dict.TryGetValue(key, out List<GameObject> list)) // benne van
                    list.Add(value);
                else
                    dict.Add(key, new List<GameObject>() { value });
            }
        }

        private Car ReadCar(byte[] data)
        {
            // skip data[0] because it is category
            byte playerId = data[1];
            byte speed = (byte)(data[2] - 128 + 15);
            byte xHigh = data[3];
            byte xLow = data[4];
            byte yHigh = data[5];
            byte yLow = data[6];
            byte nextCheckpointId = data[7];
            byte hp = data[8];
            byte mineCount = data[9];
            byte rocketCount = data[10];
            byte angle = data[11];
            byte desiredAngle = data[12];

            int x = MergeIntFragments(xHigh, xLow);
            int y = MergeIntFragments(yHigh, yLow);

            return new Car(x, y, playerId, speed, nextCheckpointId, hp, mineCount, rocketCount, angle, desiredAngle);
        }

        private Bomb ReadBomb(byte[] data)
        {
            // skip data[0] because it is category
            byte playerId = data[1];
            byte speed = 0;
            byte xHigh = data[3];
            byte xLow = data[4];
            byte yHigh = data[5];
            byte yLow = data[6];

            int x = MergeIntFragments(xHigh, xLow);
            int y = MergeIntFragments(yHigh, yLow);

            return new Bomb(x, y, playerId, speed);
        }

        private Rocket ReadRocket(byte[] data)
        {
            // skip data[0] because it is category
            byte playerId = data[1];
            byte speed = 15;
            byte xHigh = data[3];
            byte xLow = data[4];
            byte yHigh = data[5];
            byte yLow = data[6];

            int x = MergeIntFragments(xHigh, xLow);
            int y = MergeIntFragments(yHigh, yLow);

            return new Rocket(x, y, playerId, speed);
        }

        private static int MergeIntFragments(byte high, byte low) => (high << 8) + low;

        private void WipeExpiredSequenceDatas()
        {
            for (int i = 0; i < sequenceDatas.Length; i++)
            {
                SequenceData sqd = sequenceDatas[i];
                if (sqd == null)
                    continue;

                sqd.Age++;
                if (sqd.Age >= sqd.ExpireAge)
                {
                    sequenceDatas[i] = null;
                    _logger.Warning("SequenceData {id} reached maximum age of {mage}, wiping...", i, sqd.ExpireAge);
                }
            }
        }

        public void Dispose() => _udpListener.Dispose();
    }
}